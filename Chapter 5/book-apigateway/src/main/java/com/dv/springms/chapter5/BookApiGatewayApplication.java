package com.dv.springms.chapter5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
@EnableSwagger2
public class BookApiGatewayApplication {

	@Bean
	public CustomZuulFilter customZuulFilter() {
		return new CustomZuulFilter();
	}

	public static void main(String[] args) {
		SpringApplication.run(BookApiGatewayApplication.class, args);
	}
}
