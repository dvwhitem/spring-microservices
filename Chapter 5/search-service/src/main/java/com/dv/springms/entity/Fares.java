package com.dv.springms.entity;

import javax.persistence.*;

/**
 * Created by vitaliy on 5/4/17.
 */
@Entity
public class Fares {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "fare_id")
    private Long id;

    private String fare;
    private String currency;

    public Fares() {
    }

    public Fares(String fare, String currency) {
        this.fare = fare;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Fares{" +
                "id=" + id +
                ", fare='" + fare + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }
}
