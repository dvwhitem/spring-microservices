package com.dv.springms.controller;

import com.dv.springms.component.SearchComponent;
import com.dv.springms.dto.SearchQuery;
import com.dv.springms.entity.Flight;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vitaliy on 5/4/17.
 */
@RefreshScope
@CrossOrigin
@RestController
@RequestMapping("/search")
public class SearchRestController {

    private static final Logger logger = LoggerFactory.getLogger(SearchComponent.class);

    private SearchComponent searchComponent;

    @Value("${originairports.shutdown}")
    private String originAirportShutdownList;

    GaugeService gaugeService;

    public TPMCounter counter;

    @Autowired
    public SearchRestController(SearchComponent searchComponent, GaugeService gaugeService) {
        this.searchComponent = searchComponent;
        this.gaugeService = gaugeService;
        this.counter = new TPMCounter();
    }

    @RequestMapping(value="/get", method = RequestMethod.POST)
    List<Flight> search(@Valid @RequestBody SearchQuery query, HttpServletRequest request){
        logger.info("Input : "+ query);
        logger.info("Header : " + request.getHeader("custom-header"));

        if(Arrays.asList(originAirportShutdownList.split(",")).contains(query.getOrigin())){
            logger.info("The origin airport is in shutdown state");
            return new ArrayList<Flight>();
        }
        counter.increment();
        gaugeService.submit("tpm", counter.count.intValue());

        return searchComponent.search(query);
    }

    @GetMapping("/user")
    public Principal getCurrentLoggedUser(Principal user) {

        Authentication a = SecurityContextHolder.getContext().getAuthentication();

        String clientId = ((OAuth2Authentication) a).getOAuth2Request().getClientId();

        logger.info("Client id : " + clientId);
        return user;
    }
}
