package com.dv.springms.repository;

import com.dv.springms.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by vitaliy on 5/4/17.
 */
public interface FlightRepository extends JpaRepository<Flight, Long> {

    List<Flight> findByOriginAndDestinationAndFlightDate(String origin, String destination, String flightDate);

    Flight findByFlightNumberAndFlightDate(String flightNumber, String flightDate);
}
