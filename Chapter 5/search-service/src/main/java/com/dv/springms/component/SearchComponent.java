package com.dv.springms.component;

import com.dv.springms.dto.SearchQuery;
import com.dv.springms.entity.Flight;
import com.dv.springms.entity.Inventory;
import com.dv.springms.repository.FlightRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vitaliy on 5/4/17.
 */
@Component
public class SearchComponent {

    private FlightRepository flightRepository;

    private static final Logger logger = LoggerFactory.getLogger(SearchComponent.class);

    @Autowired
    public SearchComponent(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    public List<Flight> search(SearchQuery searchQuery) {

        List<Flight> searchResult = new ArrayList<>();

        List<Flight> flights = flightRepository.findByOriginAndDestinationAndFlightDate(
                searchQuery.getOrigin(),
                searchQuery.getDestination(),
                searchQuery.getFlightDate()
        );

        searchResult.addAll(flights);

        flights.forEach(flight -> {

            flight.getFares();

            int inv = flight.getInventory().getCount();

            if(inv < 0) {
                searchResult.remove(flight);
            }
        });

        return searchResult;
    }

    public void updateInventory(String flightNumber, String flightDate, int inventory) {
        logger.info("Updating inventory for flight "+ flightNumber + " innventory "+ inventory);
        Flight flight = flightRepository.findByFlightNumberAndFlightDate(flightNumber,flightDate);
        Inventory inv = flight.getInventory();
        inv.setCount(inventory);
        flightRepository.save(flight);
    }

}
