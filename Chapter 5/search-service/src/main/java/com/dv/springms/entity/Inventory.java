package com.dv.springms.entity;

import javax.persistence.*;

/**
 * Created by vitaliy on 5/4/17.
 */
@Entity
public class Inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "inv_id")
    private Long id;

    private Integer count;

    public Inventory() {
    }

    public Inventory(Integer count) {
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "id=" + id +
                ", count=" + count +
                '}';
    }
}
