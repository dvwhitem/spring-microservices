package com.dv.springms.controller;

import java.util.Calendar;
import java.util.concurrent.atomic.LongAdder;

/**
 * Created by vitaliy on 5/4/17.
 */
public class TPMCounter {

    LongAdder count;
    Calendar expiry = null;

    TPMCounter(){
        reset();
    }

    void reset (){
        count = new LongAdder();
        expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE, 1);
    }

    boolean isExpired(){
        return Calendar.getInstance().after(expiry);
    }


    void increment(){
        if(isExpired()){
            reset();
        }
        count.increment();
    }


}
