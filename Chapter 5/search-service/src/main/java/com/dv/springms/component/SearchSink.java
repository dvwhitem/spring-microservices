package com.dv.springms.component;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

/**
 * Created by vitaliy on 5/4/17.
 */
interface SearchSink {

    public static String INVENTORYQ="inventoryQ";

    @Input("inventoryQ")
    public MessageChannel inventoryQ();

}
