package com.dv.springms.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by vitaliy on 5/4/17.
 */
@Component
@EnableBinding(SearchSink.class)
public class Receiver {

    @Autowired
    SearchComponent searchComponent;

    public Receiver() {
    }

    @ServiceActivator(inputChannel = SearchSink.INVENTORYQ)
    public void accept(Map<String, Object> fare) {
        searchComponent.updateInventory(
                (String) fare.get("FLIGHT_NUMBER"),
                (String) fare.get("FLIGHT_DATE"),
                (int) fare.get("NEW_INVENTORY"));
    }
}
