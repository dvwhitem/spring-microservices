DELETE FROM fares;
DELETE FROM inventory;
DELETE FROM flight;

ALTER TABLE fares AUTO_INCREMENT = 1;
ALTER TABLE inventory AUTO_INCREMENT = 1;

INSERT INTO fares(`currency`, `fare`) VALUES("100", "USD");
INSERT INTO fares(`currency`, `fare`) VALUES("101", "USD");
INSERT INTO fares(`currency`, `fare`) VALUES("102", "USD");
INSERT INTO fares(`currency`, `fare`) VALUES("103", "USD");
INSERT INTO fares(`currency`, `fare`) VALUES("104", "USD");
INSERT INTO fares(`currency`, `fare`) VALUES("105", "USD");
INSERT INTO fares(`currency`, `fare`) VALUES("106", "USD");
INSERT INTO fares(`currency`, `fare`) VALUES("107", "USD");

INSERT INTO inventory(`count`) VALUES("100");
INSERT INTO inventory(`count`) VALUES("100");
INSERT INTO inventory(`count`) VALUES("100");
INSERT INTO inventory(`count`) VALUES("100");
INSERT INTO inventory(`count`) VALUES("100");
INSERT INTO inventory(`count`) VALUES("100");
INSERT INTO inventory(`count`) VALUES("100");
INSERT INTO inventory(`count`) VALUES("100");

INSERT  INTO flight(`flight_number`, `origin`, `destination`, `flight_date`, `fare_id`, `inv_id`)
VALUES("BF100", "SEA", "SFO", "22-JAN-16", 1, 1);

INSERT  INTO flight(`flight_number`, `origin`, `destination`, `flight_date`, `fare_id`, `inv_id`)
VALUES("BF101", "NYC", "SFO", "22-JAN-16", 2, 2);

INSERT  INTO flight(`flight_number`, `origin`, `destination`, `flight_date`, `fare_id`, `inv_id`)
VALUES("BF102", "NYC", "SFO", "22-JAN-16", 3, 3);

INSERT  INTO flight(`flight_number`, `origin`, `destination`, `flight_date`, `fare_id`, `inv_id`)
VALUES("BF103", "NYC", "SFO", "22-JAN-16", 4, 4);

INSERT  INTO flight(`flight_number`, `origin`, `destination`, `flight_date`, `fare_id`, `inv_id`)
VALUES("BF104", "CHI", "SFO", "22-JAN-16", 5, 5);

INSERT  INTO flight(`flight_number`, `origin`, `destination`, `flight_date`, `fare_id`, `inv_id`)
VALUES("BF105", "HOU", "SFO", "22-JAN-16", 6, 6);

INSERT  INTO flight(`flight_number`, `origin`, `destination`, `flight_date`, `fare_id`, `inv_id`)
VALUES("BF106", "HOU", "SFO", "22-JAN-16", 7, 7);

INSERT  INTO flight(`flight_number`, `origin`, `destination`, `flight_date`, `fare_id`, `inv_id`)
VALUES("BF107", "LAX", "SFO", "22-JAN-16", 8, 8);