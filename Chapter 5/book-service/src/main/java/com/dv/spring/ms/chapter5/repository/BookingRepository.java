package com.dv.spring.ms.chapter5.repository;

import com.dv.spring.ms.chapter5.entity.BookingRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<BookingRecord, Long> {
}
