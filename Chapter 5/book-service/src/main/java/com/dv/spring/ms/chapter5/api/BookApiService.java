package com.dv.spring.ms.chapter5.api;

import com.dv.spring.ms.chapter5.component.BookingComponent;
import com.dv.spring.ms.chapter5.component.CheckInServiceProxy;
import com.dv.spring.ms.chapter5.component.FareServiceProxy;
import com.dv.spring.ms.chapter5.dto.CheckInRecord;
import com.dv.spring.ms.chapter5.dto.Fare;
import com.dv.spring.ms.chapter5.dto.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.stereotype.Service;

/**
 * Created by vitaliy on 6/5/17.
 */
@Service
@EnableFeignClients
public class BookApiService {

    private static final Logger logger = LoggerFactory.getLogger(BookingComponent.class);

    FareServiceProxy fareServiceProxy;

    CheckInServiceProxy checkInServiceProxy;

    public BookApiService() {
    }

    @Autowired
    public BookApiService(FareServiceProxy fareServiceProxy, CheckInServiceProxy checkInServiceProxy) {
        this.fareServiceProxy = fareServiceProxy;
        this.checkInServiceProxy = checkInServiceProxy;
    }

    public Fare getFare(String flightNumber, String flightDate) {
        return fareServiceProxy.getFare(flightNumber,flightDate);
    }

    public CheckInRecord getCheckin(String id) {
        return checkInServiceProxy.getCheckIn(id);
    }

    public User getFaresUser() {
       return fareServiceProxy.getFaresUser();
    }
}
