package com.dv.spring.ms.chapter5.component;

import com.dv.spring.ms.chapter5.dto.CheckInRecord;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by vitaliy on 6/24/17.
 */
@FeignClient(name = "checkin-apigateway")
public interface CheckInServiceProxy {

    @RequestMapping(value = "/api/checkin/get/{id}", method = RequestMethod.GET)
    CheckInRecord getCheckIn(@PathVariable(value = "id") String id);
}
