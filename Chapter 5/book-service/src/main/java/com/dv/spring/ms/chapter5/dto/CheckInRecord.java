package com.dv.spring.ms.chapter5.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * Created by vitaliy on 6/24/17.
 */
@Data
@ToString
public class CheckInRecord {

    private Long id;

    private String lastName;

    private String firstName;

    private String seatNumber;

    private Date checkInTime;

    private String flightNumber;

    private String flightDate;

    private Long bookingId;
}
