package com.dv.spring.ms.chapter5.dto;

import lombok.Data;
import lombok.ToString;

/**
 * Created by vitaliy on 6/5/17.
 */
@Data
@ToString
public class User {

    private String principal;
}
