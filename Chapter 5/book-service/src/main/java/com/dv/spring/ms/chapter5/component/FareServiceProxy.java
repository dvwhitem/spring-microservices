package com.dv.spring.ms.chapter5.component;

import com.dv.spring.ms.chapter5.dto.Fare;
import com.dv.spring.ms.chapter5.dto.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

/**
 * Created by vitaliy on 5/26/17.
 */
@FeignClient(name = "fares-apigateway")
public interface FareServiceProxy {

    @RequestMapping(value = "/api/fares/get", method = RequestMethod.GET)
    Fare getFare(@RequestParam(value = "flightNumber") String flightNumber, @RequestParam(value = "flightDate") String flightDate);

    @RequestMapping(value = "/api/fares/user")
    User getFaresUser();
}
