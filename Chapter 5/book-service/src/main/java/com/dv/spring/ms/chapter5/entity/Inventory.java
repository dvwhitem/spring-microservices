package com.dv.spring.ms.chapter5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "booking_inventory")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Inventory {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String flightNumber;
    private String flightDate;
    private int available;

    public boolean isAvailable(int count){
        return ((available-count) >5);
    }

    public int getBookableInventory(){
        return available - 5;
    }
}
