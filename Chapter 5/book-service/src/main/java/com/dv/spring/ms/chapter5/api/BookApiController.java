package com.dv.spring.ms.chapter5.api;

import com.dv.spring.ms.chapter5.dto.CheckInRecord;
import com.dv.spring.ms.chapter5.dto.Fare;
import com.dv.spring.ms.chapter5.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by vitaliy on 6/5/17.
 */
@RestController
@RequestMapping("/booking")
public class BookApiController {

    BookApiService bookApiService;

    @Autowired
    public BookApiController(BookApiService bookApiService) {
        this.bookApiService = bookApiService;
    }

    @GetMapping("/fares/get")
    public Fare getFare(@RequestParam(value = "flightNumber") String flightNumber, @RequestParam(value = "flightDate") String flightDate) {
        return  bookApiService.getFare(flightNumber, flightDate);
    }

    @GetMapping("/fares/user")
    public User getFaresUser() {
       return bookApiService.getFaresUser();
    }

    @GetMapping("/checkin/get/{id}")
    public CheckInRecord getCheckIn(@PathVariable(value = "id") String id) {
        return bookApiService.getCheckin(id);
    }
}
