package com.dv.spring.ms.chapter5.controller;

import com.dv.spring.ms.chapter5.component.BookingComponent;
import com.dv.spring.ms.chapter5.dto.Fare;
import com.dv.spring.ms.chapter5.entity.BookingRecord;
import com.dv.spring.ms.chapter5.entity.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by vitaliy on 5/26/17.
 */
@RestController
@CrossOrigin
@RequestMapping("/booking")
public class BookingController {

    private BookingComponent bookingComponent;

    @Autowired
    public BookingController(BookingComponent bookingComponent) {
        this.bookingComponent = bookingComponent;
    }

    @PostMapping("/create")
    public Inventory book(@RequestBody BookingRecord record) {

        return bookingComponent.book(record);
    }
}
