package com.dv.spring.ms.chapter5.component;

import com.dv.spring.ms.chapter5.dto.Fare;
import com.dv.spring.ms.chapter5.entity.BookingRecord;
import com.dv.spring.ms.chapter5.entity.Inventory;
import com.dv.spring.ms.chapter5.repository.BookingRepository;
import com.dv.spring.ms.chapter5.repository.InventoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Component;

import java.security.Principal;

/**
 * Created by vitaliy on 5/26/17.
 */
@Component
@EnableFeignClients
@RefreshScope
public class BookingComponent {

    private static final Logger logger = LoggerFactory.getLogger(BookingComponent.class);

    private BookingRepository bookingRepository;
    private InventoryRepository inventoryRepository;
    private FareServiceProxy fareServiceProxy;
    private Sender sender;

    public BookingComponent() {
    }

    @Autowired
    public BookingComponent(BookingRepository bookingRepository, InventoryRepository inventoryRepository, FareServiceProxy fareServiceProxy, Sender sender) {
        this.bookingRepository = bookingRepository;
        this.inventoryRepository = inventoryRepository;
        this.fareServiceProxy = fareServiceProxy;
        this.sender = sender;
    }

    public Inventory book(BookingRecord record) {

        //check record

        logger.info("calling fares to get fare");
        logger.info("fare number: " + record.getFlightNumber() + " fare date: " + record.getFlightDate());
        Fare fare = fareServiceProxy.getFare(record.getFlightNumber(), record.getFlightDate());
        //check fare
        if (!record.getFare().equals(fare.getFare()))
            throw new BookingException("fare is tampered");
        logger.info("calling inventory to get inventory");
        //check inventory
        Inventory inventory = inventoryRepository.findByFlightNumberAndFlightDate(record.getFlightNumber(), record.getFlightDate());
        logger.info("inventory: " + inventory);

//        if(!inventory.isAvailable(record.getPassengers().size())){
//            throw new BookingException("No more seats avaialble");
//        }

        return inventory;

    }
}
