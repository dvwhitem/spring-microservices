package com.dv.spring.ms.chapter5.dto;

import lombok.Data;
import lombok.ToString;

/**
 * Created by vitaliy on 5/26/17.
 */
@Data
@ToString
public class Fare {

    private String flightNumber;
    private String flightDate;
    private String fare;
}
