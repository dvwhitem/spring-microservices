DELETE from passenger;
DELETE from booking_inventory;
DELETE from booking_record;

ALTER TABLE booking_inventory AUTO_INCREMENT = 1;
ALTER TABLE passenger AUTO_INCREMENT = 1;
ALTER TABLE booking_record AUTO_INCREMENT = 1;

INSERT INTO booking_inventory(`available`, `flight_date`, `flight_number`)
  VALUES ("100", "22-JAN-16", "BF100");
INSERT INTO booking_inventory(`available`, `flight_date`, `flight_number`)
  VALUES ("100", "22-JAN-16", "BF101");
INSERT INTO booking_inventory(`available`, `flight_date`, `flight_number`)
  VALUES ("100", "22-JAN-16", "BF102");
INSERT INTO booking_inventory(`available`, `flight_date`, `flight_number`)
  VALUES ("100", "22-JAN-16", "BF103");
INSERT INTO booking_inventory(`available`, `flight_date`, `flight_number`)
  VALUES ("100", "22-JAN-16", "BF104");
INSERT INTO booking_inventory(`available`, `flight_date`, `flight_number`)
  VALUES ("100", "22-JAN-16", "BF105");
INSERT INTO booking_inventory(`available`, `flight_date`, `flight_number`)
  VALUES ("100", "22-JAN-16", "BF106");


INSERT INTO booking_record(`booking_date`, `destination`, `fare`, `flight_date`, `flight_number`, `origin`, `status`)
  VALUES (NOW(), "SFO", "101", "22-JAN-16", "BF101", "NYC", NULL);

INSERT INTO booking_record(`booking_date`, `destination`, `fare`, `flight_date`, `flight_number`, `origin`, `status`)
VALUES (NOW(), "SFO", "105", "22-JAN-16", "BF105", "HOU", NULL);

INSERT INTO booking_record(`booking_date`, `destination`, `fare`, `flight_date`, `flight_number`, `origin`, `status`)
VALUES (NOW(), "SFO", "107", "22-JAN-16", "BF107", "LAX", NULL);

INSERT INTO passenger(`first_name`, `gender`, `last_name`, `booking_id`)
  VALUES ("Gean", "Male", "Franc", 1);
INSERT INTO passenger(`first_name`, `gender`, `last_name`, `booking_id`)
  VALUES ("Carlos", "Male", "Ceszar", 2);
INSERT INTO passenger(`first_name`, `gender`, `last_name`, `booking_id`)
  VALUES ("Jeana", "Female", "Liews", 3);