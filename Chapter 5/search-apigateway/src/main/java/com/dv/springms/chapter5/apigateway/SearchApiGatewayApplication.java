package com.dv.springms.chapter5.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
@EnableSwagger2
public class SearchApiGatewayApplication {

	@Bean
	public CustomZuulFilter customFilter() {
		return new CustomZuulFilter();
	}

	public static void main(String[] args) {
		SpringApplication.run(SearchApiGatewayApplication.class, args);
	}

}
