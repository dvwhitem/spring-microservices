package com.dv.springms.repository;

import com.dv.springms.entity.CheckInRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vitaliy on 6/24/17.
 */
public interface CheckinRepository extends JpaRepository<CheckInRecord, Long> {
}
