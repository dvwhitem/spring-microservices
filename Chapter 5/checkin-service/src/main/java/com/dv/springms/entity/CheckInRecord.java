package com.dv.springms.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by vitaliy on 6/24/17.
 */
@Entity(name = "checkinrecord")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CheckInRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String lastName;

    private String firstName;

    private String seatNumber;

    private Date checkInTime;

    private String flightNumber;

    private String flightDate;

    private Long bookingId;
}
