package com.dv.springms.component;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * Created by vitaliy on 6/24/17.
 */
public interface CheckInSource {

    public static String CHECKINQ="checkInQ";

    @Output("checkInQ")
    public MessageChannel checkInQ();

}
