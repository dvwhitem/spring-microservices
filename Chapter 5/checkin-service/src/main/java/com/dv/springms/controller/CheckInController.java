package com.dv.springms.controller;

import com.dv.springms.component.CheckinComponent;
import com.dv.springms.entity.CheckInRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by vitaliy on 6/24/17.
 */
@RestController
@CrossOrigin
@RequestMapping("/checkin")
public class CheckInController {

    private  CheckinComponent checkinComponent;

    @Autowired
    public CheckInController(CheckinComponent checkinComponent) {
        this.checkinComponent = checkinComponent;
    }

    @GetMapping("/get/{id}")
    public CheckInRecord getCheckIn(@PathVariable long id) {
        return checkinComponent.getChechInRecord(id);
    }

    @PostMapping("/create")
    public Long checkIn(@RequestBody CheckInRecord checkIn) {
        return checkinComponent.checkIn(checkIn);
    }
}
