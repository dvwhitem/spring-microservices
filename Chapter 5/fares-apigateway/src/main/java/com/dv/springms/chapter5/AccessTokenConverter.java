package com.dv.springms.chapter5;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;

import java.util.Map;

/**
 * Created by vitaliy on 6/4/17.
 */
public class AccessTokenConverter extends DefaultAccessTokenConverter {

    @Override
    public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
        OAuth2Authentication authentication = super.extractAuthentication(map);
        OAuth2Request request = authentication.getOAuth2Request();
        return new OAuth2Authentication(request, authentication.getUserAuthentication());
    }
}