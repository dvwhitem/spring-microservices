package com.dv.springms.chapter5.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by vitaliy on 5/7/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SearchQuery {

    private String origin;

    private String destination;

    private String flightDate;
}
