package com.dv.springms.chapter5.domain;

import lombok.Data;

import java.util.List;

/**
 * Created by vitaliy on 5/7/17.
 */
@Data
public class UIData {

    private SearchQuery searchQuery;

    private List<Flight> flights;

    private Flight selectedFlight;

    private Passenger passenger;

    private String bookingId;
}
