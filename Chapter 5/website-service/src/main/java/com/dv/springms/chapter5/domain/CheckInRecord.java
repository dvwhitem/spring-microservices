package com.dv.springms.chapter5.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Created by vitaliy on 5/7/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CheckInRecord {

    private Long id;

    private String lastName;

    private String firstName;

    private String seatNumber;

    private Date checkInTime;

    private String flightNumber;

    private String flightDate;

    private Long bookingId;

    public CheckInRecord(String lastName, String firstName, String seatNumber, Date checkInTime, String flightNumber, String flightDate, Long bookingId) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.seatNumber = seatNumber;
        this.checkInTime = checkInTime;
        this.flightNumber = flightNumber;
        this.flightDate = flightDate;
        this.bookingId = bookingId;
    }
}
