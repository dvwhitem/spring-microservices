package com.dv.springms.chapter5.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by vitaliy on 5/7/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Passenger {

    private Long id;

    private String firstName;

    private String lastName;

    private String gender;

    @JsonIgnore
    private BookingRecord bookingRecord;

    public Passenger(String firstName, String lastName, String gender, BookingRecord bookingRecord) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.bookingRecord = bookingRecord;
    }
}
