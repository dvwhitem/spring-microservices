package com.dv.springms.chapter5.api;

import com.dv.springms.chapter5.controller.BrownFieldSiteController;
import com.dv.springms.chapter5.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vitaliy on 6/5/17.
 */
@RestController
public class ApiController {

    private static final Logger logger = LoggerFactory.getLogger(BrownFieldSiteController.class);

    @Autowired
    private OAuth2RestTemplate restTemplate;

    @GetMapping("/search/user")
    public User searchGetUser() {
        return restTemplate.getForObject(
                "http://search-apigateway/api/search/user",
                User.class);
    }

    @GetMapping("/search/get")
    public Flight[] searchByQuery(
            @RequestParam("origin") String origin,
            @RequestParam("destination") String destination,
            @RequestParam("flightDate") String flightDate) {

        SearchQuery searchQuery = new SearchQuery(origin, destination, flightDate);

        Flight[] flights =
                restTemplate.postForObject(
                        "http://search-apigateway/api/search/get",
                        searchQuery,
                        Flight[].class);

        return flights;
    }

    @GetMapping("/booking/fares/get")
    public Fare getFare(@RequestParam("flightNumber") String num, @RequestParam("flightDate") String date) {
        return
                restTemplate.
                        getForObject(
                                "http://book-apigateway/api/booking/fares/get?flightNumber={flightNumber}&flightDate={flightDate}",
                                Fare.class,
                                num,
                                date);

    }

    @GetMapping("/booking/fares/user")
    public User faresGetUser() {

        return restTemplate.getForObject(
                "http://book-apigateway/api/booking/fares/user",
                User.class);

    }

    @GetMapping("/booking/checkin/get/{id}")
    public CheckInRecord getCheckIn(@PathVariable("id") String id) {

        return restTemplate.getForObject("http://book-apigateway/api/booking/checkin/get/{id}", CheckInRecord.class, id);

    }


    @GetMapping("/booking/create")
    public BookingRecord createBookingRecord() {

        BookingRecord record = new BookingRecord();
        record.setBookingDate(new Date());
        record.setDestination("SFO");
        record.setFare("101");
        record.setFlightDate("22-JAN-16");
        record.setFlightNumber("BF101");
        record.setOrigin("NYC");

        return restTemplate.postForObject("http://book-apigateway/api/booking/create", record, BookingRecord.class);
    }

}
