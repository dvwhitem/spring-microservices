package com.dv.springms.chapter5.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.Set;

/**
 * Created by vitaliy on 5/7/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BookingRecord {

    private Long id;

    private String firstName;

    private String origin;

    private String destination;

    private String flightDate;

    private String flightNumber;

    private Date bookingDate;

    private String fare;

    private String status;

    private Set<Passenger> passengers;

    public BookingRecord(String firstName, String from, String to, String flightDate, Date bookingDate, String fare, String flightNumber) {
        this.firstName = firstName;
        this.origin = from;
        this.destination = to;
        this.flightDate = flightDate;
        this.bookingDate = bookingDate;
        this.fare = fare;
        this.flightNumber = flightNumber;
    }
}
