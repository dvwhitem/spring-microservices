package com.dv.springms.chapter5.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by vitaliy on 5/7/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Fares {

    private Long id;

    private String fare;

    private String currency;

    public Fares(String fare, String currency) {
        this.fare = fare;
        this.currency = currency;
    }
}
