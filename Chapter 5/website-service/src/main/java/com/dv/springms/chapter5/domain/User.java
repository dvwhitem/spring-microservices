package com.dv.springms.chapter5.domain;

import lombok.Data;
import lombok.ToString;

/**
 * Created by vitaliy on 6/5/17.
 */
@Data
@ToString
public class User {

    private String principal;
}
