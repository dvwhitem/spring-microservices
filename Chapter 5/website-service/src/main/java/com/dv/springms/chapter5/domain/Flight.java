package com.dv.springms.chapter5.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by vitaliy on 5/7/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Flight {

    private Long id;

    private String flightNumber;

    private String origin;

    private String destination;

    private String flightDate;

    private Fares fares;

    public Flight(String flightNumber, String origin, String destination, String flightDate, Fares fares) {
        this.flightNumber = flightNumber;
        this.origin = origin;
        this.destination = destination;
        this.flightDate = flightDate;
        this.fares = fares;
    }
}
