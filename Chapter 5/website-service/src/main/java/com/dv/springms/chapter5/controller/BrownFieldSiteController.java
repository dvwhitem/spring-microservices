package com.dv.springms.chapter5.controller;

import com.dv.springms.chapter5.domain.Fare;
import com.dv.springms.chapter5.domain.Flight;
import com.dv.springms.chapter5.domain.SearchQuery;
import com.dv.springms.chapter5.domain.UIData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vitaliy on 5/7/17.
 */
@Controller
public class BrownFieldSiteController {

    private static final Logger logger = LoggerFactory.getLogger(BrownFieldSiteController.class);

    @Autowired
    private OAuth2RestTemplate restTemplate;

    private Map<String, UIData> data = new HashMap<>();

    @GetMapping("/")
    public String index(Model model) {

        UIData uiData = new UIData();
        model.addAttribute("uidata", uiData);
        return "search";

    }

    @PostMapping("/search")
    public String searchSubmit(@ModelAttribute UIData uiData, Model model) {

        Flight[] flights =
                restTemplate.postForObject(
                        "http://search-apigateway/api/search/get",
                        uiData.getSearchQuery(),
                        Flight[].class);


        uiData.setFlights(Arrays.asList(flights));
        data.put("uidata", uiData);
        return "redirect:/search/result";
    }

    @GetMapping("/search/result")
    private String searchResult(Model model) {
        model.addAttribute("uidata", data.get("uidata"));
        return "search-result";
    }
}
