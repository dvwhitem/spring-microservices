package com.dv.spring.ms.chapter5.controller;

import com.dv.spring.ms.chapter5.component.FaresComponent;
import com.dv.spring.ms.chapter5.entity.Fare;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


/**
 * Created by vitaliy on 5/25/17.
 */
@RestController
@CrossOrigin
@RequestMapping("/fares")
public class FaresController {

    private static final Logger logger = LoggerFactory.getLogger(FaresController.class);

    FaresComponent faresComponent;

    @Autowired
    public FaresController(FaresComponent faresComponent) {
        this.faresComponent = faresComponent;
    }

    @GetMapping("/get")
    public Fare getFare(
            @RequestParam(value = "flightNumber") String flightNumber,
            @RequestParam(value = "flightDate") String flightDate) {

        return faresComponent.getFare(flightNumber, flightDate);

    }

    @GetMapping("/user")
    public Principal getCurrentLoggedUser(Principal user) {

        Authentication a = SecurityContextHolder.getContext().getAuthentication();

        OAuth2Request oAuth2Request = ((OAuth2Authentication) a).getOAuth2Request();

        logger.info("clientId : " + oAuth2Request.getClientId());
        logger.info("scope : " + oAuth2Request.getScope());

        return user;
    }

}
