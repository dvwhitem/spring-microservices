package com.dv.spring.ms.chapter5.component;

import com.dv.spring.ms.chapter5.entity.Fare;
import com.dv.spring.ms.chapter5.repository.FaresRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Created by vitaliy on 5/25/17.
 */
@Component
public class FaresComponent {

    private static final Logger logger = LoggerFactory.getLogger(FaresComponent.class);

    private FaresRepository faresRepository;

    public FaresComponent() {
    }

    @Autowired
    public FaresComponent(FaresRepository faresRepository) {
        this.faresRepository = faresRepository;
    }

    public Fare getFare(String flightNumber, String flightDate) {

        logger.info("Looking for fares flightNumber "+ flightNumber + " flightDate "+ flightDate);

        Fare fare =  faresRepository.getFareByFlightNumberAndFlightDate(flightNumber,flightDate);

        logger.info("Result: " + fare.getFare() + " " + fare.getFlightNumber() + " " + fare.getFlightDate());

        return fare;
    }
}
