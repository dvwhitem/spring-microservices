package com.dv.spring.ms.chapter5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by vitaliy on 5/25/17.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Fare implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String flightNumber;

    private String flightDate;

    private String fare;
}
