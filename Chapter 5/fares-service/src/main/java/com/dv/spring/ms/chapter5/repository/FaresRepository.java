package com.dv.spring.ms.chapter5.repository;

import com.dv.spring.ms.chapter5.entity.Fare;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vitaliy on 5/25/17.
 */
public interface FaresRepository extends JpaRepository<Fare, Long> {

    Fare getFareByFlightNumberAndFlightDate(String flightNumber, String flightDate);

}
