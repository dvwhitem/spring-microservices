package com.dv.spring.ms.chapter5.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

/**
 * Created by vitaliy on 5/26/17.
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http
                .anonymous().disable()
                .requestMatchers().antMatchers("/fares/**")
                .and().authorizeRequests()
                .antMatchers("/fares/**").access("#oauth2.hasScope('server')");
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer config) {
        config.tokenServices(tokenService());
    }

    @Bean
    @ConfigurationProperties("security.oauth2.client")
    public RemoteTokenServices tokenService() {
        RemoteTokenServices tokenService = new RemoteTokenServices();
        tokenService.setAccessTokenConverter(defaultAccessTokenConverter());
        return tokenService;
    }

    @Bean
    DefaultAccessTokenConverter defaultAccessTokenConverter(){
        return new AccessTokenConverter();
    }

}
