package com.dv.spring.ms.chapter5.config;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;

import java.util.Map;

/**
 * Created by vitaliy on 5/28/17.
 */
public class AccessTokenConverter extends DefaultAccessTokenConverter {

    @Override
    public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
        OAuth2Authentication authentication = super.extractAuthentication(map);
        OAuth2Request request = authentication.getOAuth2Request();
        return new OAuth2Authentication(request, authentication.getUserAuthentication());
    }
}