package com.dv.springms.chapter5.repository;

import com.dv.springms.chapter5.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by vitaliy on 5/9/17.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findOneByEmail(String email);
}
