package com.dv.springms.chapter5.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by vitaliy on 5/8/17.
 */
@RestController
public class UserController {

    @GetMapping("/user")
    public Principal getCurrentLoggedUser(Principal user) {
        return user;
    }
}
