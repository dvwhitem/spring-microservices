package com.dv.springms.chapter5.dto;

import com.dv.springms.chapter5.entity.User;
import lombok.Data;
import lombok.ToString;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.stream.Collectors;

/**
 * Created by vitaliy on 5/9/17.
 */
@Data
@ToString
public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public CurrentUser(User user) {
        super(
                user.getEmail(),
                user.getPassword(),
                AuthorityUtils.createAuthorityList(
                        user.getRoles()
                                .stream()
                                .map(role -> role.getName())
                                .collect(Collectors.toList())
                                .toArray(new String[0])
                )
        );


        this.user = user;
    }


    public Long getId() {
        return user.getId();
    }

}
