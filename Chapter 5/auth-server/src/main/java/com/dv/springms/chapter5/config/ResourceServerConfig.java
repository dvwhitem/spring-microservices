package com.dv.springms.chapter5.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Created by vitaliy on 5/8/17.
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http
                .requestMatchers()
                .antMatchers("/user")
                .and().authorizeRequests()
                .antMatchers("/resources/**", "/login").permitAll()
                .antMatchers("/user").access("#oauth2.hasScope('ui')");
    }
}