package com.dv.springms.chapter5.service.impl;

import com.dv.springms.chapter5.dto.CurrentUser;
import com.dv.springms.chapter5.entity.Role;
import com.dv.springms.chapter5.entity.User;
import com.dv.springms.chapter5.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by vitaliy on 5/9/17.
 */
@Service
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    private UserService userService;

    @Autowired
    public CustomUserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userService.getUserByEmail(email).orElseThrow(()
                -> new UsernameNotFoundException(String.format("User with email= %s was not found", email)));



        return new CurrentUser(user);
    }
}
