package com.dv.springms.chapter5.service;

import com.dv.springms.chapter5.entity.User;

import java.util.Optional;

/**
 * Created by vitaliy on 5/9/17.
 */
public interface UserService {

    Optional<User> getUserByEmail(String email);
}
