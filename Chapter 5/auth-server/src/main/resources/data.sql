DELETE FROM user_roles;
DELETE FROM users;
DELETE FROM roles;


ALTER TABLE users AUTO_INCREMENT = 1;
ALTER TABLE roles AUTO_INCREMENT = 1;

INSERT INTO users(`email`, `password`) VALUES ('dvwhitem@localhost', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO user_roles(role_id, user_id) VALUES(1,1);
INSERT INTO user_roles(role_id, user_id) VALUES(2,1);

