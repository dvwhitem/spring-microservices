package com.dv.springms.chapter2.web;

import com.dv.springms.chapter2.component.TPSHealth;
import com.dv.springms.chapter2.model.Greet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 4/6/17.
 */
@RestController
public class GreetingController {

    private static final Logger logger = LoggerFactory.getLogger(GreetingController.class);

    TPSHealth health;
    CounterService counterService;
    GaugeService gaugeService;

    @Autowired
    public GreetingController(TPSHealth health, CounterService counterService, GaugeService gaugeService) {
        this.health = health;
        this.counterService = counterService;
        this.gaugeService = gaugeService;
    }

    @Autowired


    @CrossOrigin
    @GetMapping("/greeting")
    Greet greet() {
        logger.info("Serving Request...!!!");
        health.updateTx();
        this.counterService.increment("greet.txnCount");
        gaugeService.submit("greet.customgauge", 1.0);
        return new Greet("Hello World!");
    }
}
