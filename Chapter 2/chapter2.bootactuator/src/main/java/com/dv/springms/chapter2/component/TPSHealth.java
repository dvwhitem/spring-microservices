package com.dv.springms.chapter2.component;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * Created by vitaliy on 4/7/17.
 */
@Component
public class TPSHealth implements HealthIndicator {

    TPSCounter counter;

    @Override
    public Health health() {

        boolean health = counter.isWeak();
        if(health) {
            return Health.outOfService().withDetail("Too many requests", "OutOfService").build();
        }

        return Health.up().build();
    }

    public  void updateTx() {
        if(counter == null || counter.isExpired()) {
            counter = new TPSCounter();
        }
        counter.increment();
    }

}
