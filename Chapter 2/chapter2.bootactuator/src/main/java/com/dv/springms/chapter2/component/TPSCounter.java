package com.dv.springms.chapter2.component;

import java.util.Calendar;
import java.util.concurrent.atomic.LongAdder;

/**
 * Created by vitaliy on 4/7/17.
 */
public class TPSCounter {

    LongAdder count;
    int threshold = 2;
    Calendar expiry = null;

    public TPSCounter() {
        count = new LongAdder();
        expiry = Calendar.getInstance();
        expiry.add(Calendar.MINUTE,1);
    }

    public boolean isExpired() {
        return Calendar.getInstance().after(expiry);
    }

    public boolean isWeak() {
        return  (count.intValue() > threshold);
    }

    public void increment() {
        count.increment();
    }
}
