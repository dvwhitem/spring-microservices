package com.dv.springms.chapter2.web;

import com.dv.springms.chapter2.model.Greet;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 4/2/17.
 */
@RestController
public class HomeController {

    @GetMapping("/")
    public Greet greet() {
        return new Greet("Hello World");
    }
}
