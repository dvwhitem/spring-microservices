package com.dv.springms.chapter2.model;

/**
 * Created by vitaliy on 4/2/17.
 */
public class Greet {

    private String message;

    public Greet() {
    }

    public Greet(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
