package com.dv.springms.chapter2;

import com.dv.springms.chapter2.model.Greet;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.servlet.annotation.WebInitParam;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class BootrestApplicationTests {

	@Test
	public void testVanillaService() {
		RestTemplate restTemplate = new RestTemplate();
		Greet greet = restTemplate.getForObject("http://localhost:8080", Greet.class);
		Assert.assertEquals("Hello World", greet.getMessage());
	}

}
