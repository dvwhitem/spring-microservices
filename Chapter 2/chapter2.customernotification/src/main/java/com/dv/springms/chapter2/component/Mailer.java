package com.dv.springms.chapter2.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * Created by vitaliy on 4/5/17.
 */
@Component
public class Mailer {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendMail(String email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("Registration");
        message.setText("Successfully Registered");
        javaMailSender.send(message);
    }
}
