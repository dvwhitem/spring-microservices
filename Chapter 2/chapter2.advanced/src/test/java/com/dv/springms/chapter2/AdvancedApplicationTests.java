package com.dv.springms.chapter2;

import com.dv.springms.chapter2.model.Greet;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AdvancedApplicationTests {


	//@Test
	public void TestVanillaService() {
		RestTemplate restTemplate = new RestTemplate();

		Greet greet = restTemplate.getForObject("http://localhost:8080", Greet.class);
		Assert.assertEquals("Hello Spring Boot Security", greet.getMessage());
	}


	//@Test
	public void testSecureService() {
		String plainCredentials = "guest:guest12345";
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Authorization", "Basic " + new String(Base64.encode(plainCredentials.getBytes())));
		HttpEntity<String> request = new HttpEntity<String>(httpHeaders);
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Greet> entity = restTemplate.exchange("http://localhost:8080", HttpMethod.GET, request, Greet.class);
		Assert.assertEquals("Hello Spring Boot Security", entity.getBody().getMessage());
	}

	@Test
	public void testOAuthService() {
		ResourceOwnerPasswordResourceDetails details = new ResourceOwnerPasswordResourceDetails();
		details.setUsername("guest");
		details.setPassword("guest12345");
		details.setAccessTokenUri("http://localhost:8080/oauth/token");
		details.setClientId("trustedclient");
		details.setClientSecret("trustedclient12345");
		details.setGrantType("password");

		DefaultOAuth2ClientContext context = new DefaultOAuth2ClientContext();
		OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(details,context);

		Greet greet = restTemplate.getForObject("http://localhost:8080", Greet.class);
		Assert.assertEquals("Hello Spring Boot Security", greet.getMessage());
	}
}
