package com.dv.springms.chapter2.web;

import com.dv.springms.chapter2.model.Greet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 4/2/17.
 */
@RestController
public class GreetController {

    private static final Logger logger = LoggerFactory.getLogger(GreetController.class);

    Environment environment;

    @Autowired
    public GreetController(Environment environment) {
        this.environment = environment;
    }

    @CrossOrigin
    @GetMapping("/")
    Greet greet() {
        logger.info("bootrest.customproperty  " + environment.getProperty("bootrest.customproperty"));
        return new Greet("Hello Spring Boot Security");
    }
}
