package com.dv.springms.chapter2;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Created by vitaliy on 4/3/17.
 */
@Component
public class Receiver {

    @RabbitListener(queues = "TestQ")
    public void processMessage(String content) {
        System.out.println("content = [" + content + "]");
    }

}
