package com.dv.springms.chapter2.web;

import com.dv.springms.chapter2.component.CustomerRegister;
import com.dv.springms.chapter2.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 4/5/17.
 */
@RestController
public class CustomerController {

    @Autowired
    CustomerRegister customerRegister;

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    Customer register(@RequestBody Customer customer) {
        return customerRegister.register(customer);
    }
}
