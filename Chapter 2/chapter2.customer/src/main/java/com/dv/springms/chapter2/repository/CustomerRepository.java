package com.dv.springms.chapter2.repository;

import com.dv.springms.chapter2.domain.Customer;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

/**
 * Created by vitaliy on 4/5/17.
 */
@RepositoryRestResource
@Lazy
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Optional<Customer> findByName(@Param("name") String name);
}
