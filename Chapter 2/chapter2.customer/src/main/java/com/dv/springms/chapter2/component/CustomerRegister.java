package com.dv.springms.chapter2.component;

import com.dv.springms.chapter2.domain.Customer;
import com.dv.springms.chapter2.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by vitaliy on 4/5/17.
 */
@Component
@Lazy
public class CustomerRegister {

    CustomerRepository repository;
    Sender sender;

    @Autowired
    public CustomerRegister(CustomerRepository repository, Sender sender) {
        this.repository = repository;
        this.sender = sender;
    }

    public Customer register(Customer customer) {
        Optional<Customer> existingCustomer = repository.findByName(customer.getName());
        if(existingCustomer.isPresent()) {
            throw new RuntimeException("is alredy exists");
        } else {
            repository.save(customer);
            sender.send(customer.getEmail());
        }
        return customer;
    }
}
