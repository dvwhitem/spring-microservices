package com.dv.springms.chapter2;

import com.dv.springms.chapter2.domain.Customer;
import com.dv.springms.chapter2.repository.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class CustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerApplication.class, args);
	}

	@Bean
	CommandLineRunner init(CustomerRepository repository) {
		return (evt)->{
			repository.save(new Customer("Lucas", "lucas@boot.com"));
			repository.save(new Customer("Rico", "rico@boot.com"));
			repository.save(new Customer("Sergio", "sergio@boot.com"));
			repository.save(new Customer("Havier", "havier@boot.com"));
			repository.save(new Customer("Adam", "adam@boot.com"));
			repository.save(new Customer("Laurence", "laurence@boot.com"));
			repository.save(new Customer("Abel", "abel@boot.com"));
			repository.save(new Customer("Fernando", "fernando@boot.com"));
			repository.save(new Customer("Gareth", "gareth@boot.com"));
		};
	}
}
