package com.dv.springms.chapter2.web;

import com.dv.springms.chapter2.model.Greet;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by vitaliy on 4/2/17.
 */
@RestController
public class HomeController {

    @GetMapping("/")
    Greet greet() {
        return new Greet("Hello World");
    }

    @GetMapping("/greeting")
    public HttpEntity<Greet> greeting(@RequestParam(value = "name", required = false, defaultValue = "Hateoas") String name) {
        Greet greet = new Greet("Hello " + name);
        greet.add(linkTo(methodOn(HomeController.class).greeting(name)).withSelfRel());
        return new ResponseEntity<Greet>(greet, HttpStatus.OK);
    }
}
