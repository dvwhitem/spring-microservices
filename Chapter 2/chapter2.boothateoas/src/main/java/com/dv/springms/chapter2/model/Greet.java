package com.dv.springms.chapter2.model;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by vitaliy on 4/2/17.
 */
public class Greet extends ResourceSupport {

    private String message;

    public Greet() {
    }

    public Greet(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Greet{" +
                "message='" + message + '\'' +
                '}';
    }
}
