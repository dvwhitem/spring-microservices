package com.dv.spring.ms.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 4/8/17.
 */
@RestController
public class GreetController {

    @RequestMapping("/")
    public String greet() {
        return "Hello World!";
    }
}
