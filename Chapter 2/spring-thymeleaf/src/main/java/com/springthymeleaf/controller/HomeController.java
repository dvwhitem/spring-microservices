package com.springthymeleaf.controller;

import com.springthymeleaf.model.Greet;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 4/1/17.
 */
@RestController
public class HomeController {

    @GetMapping("/rest")
    public Greet sayHello() {
        return new Greet("Hello Rest Controller");
    }
}
