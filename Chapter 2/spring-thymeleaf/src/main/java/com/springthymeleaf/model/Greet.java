package com.springthymeleaf.model;

/**
 * Created by vitaliy on 4/1/17.
 */
public class Greet {

    private String message;

    public Greet(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
