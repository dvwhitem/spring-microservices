## Spring Boot Microservices

* Chapter 2
    *  **Spring Thymeleaf** is a sample Spring project with Thymeleaf
    *  **Spring Boot Rest** is a sample rest app by Spring Boot
    *  **Spring Boot HATEOAS** is a hypermedia-driven site provides information to navigate the site's REST interfaces dynamically by including hypermedia links with the responses
    *  **Spring Security OAuth2** is as sample demo app used spring security (basic authorization) and Spring OAuth 