package com.dv.springms.chapter4.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by vitaliy on 4/19/17.
 */
@Entity
public class CheckInRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String lastName;

    private String firstNumber;

    private String seatNumber;

    private Date checkInDate;

    private String flightNumber;

    private String flightDate;

    private Long bookingId;

    public CheckInRecord() {
    }

    public CheckInRecord(String lastName, String firstNumber, String seatNumber, Date checkInDate, String flightNumber, String flightDate, Long bookingId) {
        this.lastName = lastName;
        this.firstNumber = firstNumber;
        this.seatNumber = seatNumber;
        this.checkInDate = checkInDate;
        this.flightNumber = flightNumber;
        this.flightDate = flightDate;
        this.bookingId = bookingId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(String firstNumber) {
        this.firstNumber = firstNumber;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(String flightDate) {
        this.flightDate = flightDate;
    }

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    @Override
    public String toString() {
        return "CheckInRecord{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstNumber='" + firstNumber + '\'' +
                ", seatNumber='" + seatNumber + '\'' +
                ", checkInDate=" + checkInDate +
                ", flightNumber='" + flightNumber + '\'' +
                ", flightDate='" + flightDate + '\'' +
                ", bookingId=" + bookingId +
                '}';
    }
}
