package com.dv.springms.chapter4.component;

import com.dv.springms.chapter4.domain.CheckInRecord;
import com.dv.springms.chapter4.repository.CheckinRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by vitaliy on 4/19/17.
 */
@Component
public class CheckinComponent {

    private static final Logger logger = LoggerFactory.getLogger(CheckinComponent.class);

    CheckinRepository checkinRepository;

    Sender sender;

    @Autowired
    public CheckinComponent(CheckinRepository checkinRepository, Sender sender) {
        this.checkinRepository = checkinRepository;
        this.sender = sender;
    }

    public Long checkIn(CheckInRecord checkIn) {
        checkIn.setCheckInDate(new Date());
        logger.info("Saving checkin");
        //save
        Long id = checkinRepository.save(checkIn).getId();
        logger.info("Successfully saved checkin ");
        //send a message back to booking to update status
        logger.info("Sending booking id "+ id);
        sender.send(id);
        return  id;
    }

    public CheckInRecord getCheckInRecord(Long id) {
        return checkinRepository.getOne(id);
    }
}
