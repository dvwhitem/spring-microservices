package com.dv.springms.chapter4.controller;

import com.dv.springms.chapter4.component.CheckinComponent;
import com.dv.springms.chapter4.domain.CheckInRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by vitaliy on 4/19/17.
 */
@RestController
@CrossOrigin
@RequestMapping("/checkin")
public class CheckinController {

    CheckinComponent checkinComponent;

    @Autowired
    public CheckinController(CheckinComponent checkinComponent) {
        this.checkinComponent = checkinComponent;
    }

    @RequestMapping("/get/{id}")
    CheckInRecord getCheckIn(@PathVariable Long id) {
            return checkinComponent.getCheckInRecord(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    Long chechId(@RequestBody CheckInRecord checkInRecord) {
        return checkinComponent.checkIn(checkInRecord);
    }
}
