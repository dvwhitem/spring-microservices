package com.dv.springms.chapter4.repository;

import com.dv.springms.chapter4.domain.CheckInRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vitaliy on 4/19/17.
 */
public interface CheckinRepository extends JpaRepository<CheckInRecord, Long> {
}
