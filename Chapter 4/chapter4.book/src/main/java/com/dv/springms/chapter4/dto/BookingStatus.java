package com.dv.springms.chapter4.dto;

/**
 * Created by vitaliy on 4/20/17.
 */
public class BookingStatus {

    public static final String BOOKING_CONFIRMED = "BOOKING_CONFIRMED";

    public static final String CHECKED_IN = "CHECKED_IN";

}
