package com.dv.springms.chapter4.controller;

import com.dv.springms.chapter4.component.BookingComponent;
import com.dv.springms.chapter4.domain.BookingRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by vitaliy on 4/20/17.
 */
@RestController
@CrossOrigin
@RequestMapping("/booking")
public class BookingController {

    BookingComponent bookingComponent;

    @Autowired
    public BookingController(BookingComponent bookingComponent) {
        this.bookingComponent = bookingComponent;
    }

    @RequestMapping(value="/create" , method = RequestMethod.POST)
    long book(@RequestBody BookingRecord record){
        System.out.println("Booking Request" + record);
        return bookingComponent.book(record);
    }
    @RequestMapping("/get/{id}")
    BookingRecord getBooking(@PathVariable long id){
        return bookingComponent.getBooking(id);
    }

}
