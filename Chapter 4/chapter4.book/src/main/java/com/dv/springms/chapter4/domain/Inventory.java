package com.dv.springms.chapter4.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by vitaliy on 4/19/17.
 */
@Entity
public class Inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String flightNumber;

    private String flightDate;

    private Integer available;

    public Inventory() {
    }

    public Inventory(String flightNumber, String flightDate, Integer available) {
        this.flightNumber = flightNumber;
        this.flightDate = flightDate;
        this.available = available;
    }

    public boolean isAvailable(int count){
        return ((available-count) >5);
    }

    public int getBookableInventory(){
        return available - 5;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(String flightDate) {
        this.flightDate = flightDate;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "id=" + id +
                ", flightNumber='" + flightNumber + '\'' +
                ", flightDate='" + flightDate + '\'' +
                ", available=" + available +
                '}';
    }
}
