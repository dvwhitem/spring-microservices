package com.dv.springms.chapter4.component;

import com.dv.springms.chapter4.domain.BookingRecord;
import com.dv.springms.chapter4.domain.Inventory;
import com.dv.springms.chapter4.domain.Passenger;
import com.dv.springms.chapter4.dto.BookingStatus;
import com.dv.springms.chapter4.dto.Fare;
import com.dv.springms.chapter4.exception.BookingException;
import com.dv.springms.chapter4.repository.BookingRepository;
import com.dv.springms.chapter4.repository.InventoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by vitaliy on 4/20/17.
 */
@Component
public class BookingComponent {

    private static final Logger logger = LoggerFactory.getLogger(BookingComponent.class);

    private static final String FareURL = "http://localhost:8080/fares";

    public BookingRepository bookingRepository;

    public InventoryRepository inventoryRepository;

    private RestTemplate restTemplate;

    public Sender sender;

    @Autowired
    public BookingComponent(BookingRepository bookingRepository, InventoryRepository inventoryRepository, Sender sender) {
        this.bookingRepository = bookingRepository;
        this.inventoryRepository = inventoryRepository;
        this.restTemplate = new RestTemplate();
        this.sender = sender;
    }

    public Long book(BookingRecord record) {

        logger.info("calling fares to get fare");
        Fare fare = restTemplate.getForObject(FareURL + "/get?flightNumber="+record.getFlightNumber()+"&flightDate="+record.getFlightDate(), Fare.class);
        logger.info("calling fares to get fare "+ fare);
        //check fare
        if (!record.getFare().equals(fare.getFare())) {
            throw new BookingException("fare is tampered");
        }
        //---------------------------------------
        //check inventory
        logger.info("calling inventory to get inventory");
        Inventory inventory = inventoryRepository.findByFlightNumberAndFlightDate(record.getFlightNumber(), record.getFlightDate());
        if(!inventory.isAvailable(record.getPassengers().size())) {
            throw new BookingException("No more seats avaialble");
        }
        logger.info("successfully checked inventory" + inventory);
        logger.info("calling inventory to update inventory");
        //update inventory
        logger.info("Available: " + inventory.getAvailable() + " Passengers: " + record.getPassengers().size());
        inventory.setAvailable(inventory.getAvailable() - record.getPassengers().size());
        inventoryRepository.saveAndFlush(inventory);
        logger.info("sucessfully updated inventory");
        //---------------------------------------
        // save booking
        record.setStatus(BookingStatus.BOOKING_CONFIRMED);
        Set<Passenger> passengers = record.getPassengers();
        passengers.forEach(passenger -> {passenger.setBookingRecord(record);});
        record.setBookingDate(new Date());
        Long id = bookingRepository.save(record).getId();
        logger.info("Successfully saved booking");
        //----------------------------------------

        //send a message to search to update inventory
        logger.info("sending a booking event");
        Map<String, Object> bookingDetails = new HashMap<>();
        bookingDetails.put("FLIGHT_NUMBER", record.getFlightNumber());
        bookingDetails.put("FLIGHT_DATE", record.getFlightDate());
        bookingDetails.put("NEW_INVENTORY", inventory.getBookableInventory());
        sender.send(bookingDetails);
        logger.info("booking event successfully delivered "+ bookingDetails);
        return id;
    }

    public BookingRecord getBooking(long id) {
        logger.info("Find id : " + id);
        BookingRecord record = bookingRepository.findOne(id);
        logger.info("Rec: " + record);
        return record;
    }

    public void updateStatus(String status, long bookingId) {
        BookingRecord record = bookingRepository.findOne(bookingId);
        record.setStatus(status);
    }
}
