package com.dv.springms.chapter4.component;

import com.dv.springms.chapter4.dto.BookingStatus;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by vitaliy on 4/20/17.
 */
@Component
public class Receiver {

    BookingComponent bookingComponent;

    @Autowired
    public Receiver(BookingComponent bookingComponent) {
        this.bookingComponent = bookingComponent;
    }

    @RabbitListener(queues = "CheckINQ")
    public void processMessage(Long bookingId) {
        System.out.println("Booking ID : " + bookingId);
        bookingComponent.updateStatus(BookingStatus.CHECKED_IN, bookingId);
    }
}
