package com.dv.springms.chapter4.exception;

/**
 * Created by vitaliy on 4/20/17.
 */
public class BookingException extends RuntimeException {

    public BookingException(String message) {
        super(message);
    }
}
