package com.dv.springms.chapter4.component;

import com.sun.org.apache.regexp.internal.RE;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by vitaliy on 4/20/17.
 */
@Component
public class Sender {

    RabbitMessagingTemplate messagingTemplate;

    @Autowired
    public Sender(RabbitMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Bean
    public Queue queue() {
        return new Queue("SearchQ", false);
    }

    @Bean
    public Queue queue1() {
        return new Queue("CheckINQ", false);
    }

    public void send(Object message) {
        messagingTemplate.convertAndSend("SearchQ",message);
    }
}
