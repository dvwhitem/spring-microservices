package com.dv.springms.chapter4.repository;

import com.dv.springms.chapter4.domain.BookingRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vitaliy on 4/20/17.
 */
public interface BookingRepository extends JpaRepository<BookingRecord, Long> {
}
