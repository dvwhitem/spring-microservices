package com.dv.springms.chapter4.repository;

import com.dv.springms.chapter4.domain.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vitaliy on 4/20/17.
 */
public interface InventoryRepository extends JpaRepository<Inventory, Long> {

    Inventory findByFlightNumberAndFlightDate(String flightNumber, String flightDate);
}
