package com.dv.springms.chapter4.component;

import com.dv.springms.chapter4.domain.Flight;
import com.dv.springms.chapter4.domain.Inventory;
import com.dv.springms.chapter4.dto.SearchQuery;
import com.dv.springms.chapter4.repository.FlightRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vitaliy on 4/18/17.
 */
@Component
public class SearchComponent {

    private static final Logger logger = LoggerFactory.getLogger(SearchComponent.class);

    FlightRepository flightRepository;

    @Autowired
    public SearchComponent(FlightRepository flightRepositiry) {
        this.flightRepository = flightRepositiry;
    }

    public List<Flight> search(SearchQuery query) {

        List<Flight> flights = flightRepository.findByOriginAndDestinationAndFlightDate(
          query.getOrigin(),
          query.getDestination(),
          query.getFlightDate()
        );

        List<Flight> searchResult = new ArrayList<Flight>();

        searchResult.addAll(flights);

        flights.forEach(flight -> {
            flight.getFares();
            int inv = flight.getInventory().getCount();
            if(inv < 0) {
                searchResult.remove(flight);
            }
        });

        return searchResult;
    }

    public void updateInventory(String flightNumber, String flightDate, int inventory) {
        logger.info("Updating inventory for flight "+ flightNumber + " innventory "+ inventory);
        Flight flight = flightRepository.findByFlightNumberAndFlightDate(flightNumber,flightDate);
        Inventory inv = flight.getInventory();
        inv.setCount(inventory);
        flightRepository.save(flight);
    }
}
