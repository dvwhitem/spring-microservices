package com.dv.springms.chapter4.domain;

import javax.persistence.*;

/**
 * Created by vitaliy on 4/18/17.
 */
@Entity
public class Fares {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "fare_id")
    private Long id;

    private String fare;

    private String currency;

    public Fares() {
    }

    public Fares(String fare, String currencey) {
        this.fare = fare;
        this.currency = currencey;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public String getCurrencey() {
        return currency;
    }

    public void setCurrencey(String currencey) {
        this.currency = currencey;
    }

    @Override
    public String toString() {
        return "Fares{" +
                "id=" + id +
                ", fare='" + fare + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }
}
