package com.dv.springms.chapter4.controller;

import com.dv.springms.chapter4.component.SearchComponent;
import com.dv.springms.chapter4.domain.Flight;
import com.dv.springms.chapter4.dto.SearchQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by vitaliy on 4/18/17.
 */
@CrossOrigin
@RestController
@RequestMapping("/search")
public class SearchRestController {

    private SearchComponent searchComponent;

    @Autowired
    public SearchRestController(SearchComponent searchComponent) {
        this.searchComponent = searchComponent;
    }

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    List<Flight> search(@RequestBody SearchQuery query) {
        System.out.println("Input " + query);
        return searchComponent.search(query);
    }

}
