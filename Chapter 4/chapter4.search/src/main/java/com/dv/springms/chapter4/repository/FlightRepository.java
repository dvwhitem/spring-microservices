package com.dv.springms.chapter4.repository;

import com.dv.springms.chapter4.domain.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by vitaliy on 4/18/17.
 */
public interface FlightRepository extends JpaRepository<Flight, Long> {

    List<Flight> findByOriginAndDestinationAndFlightDate(String origin, String destination, String flightNumber);

    Flight findByFlightNumberAndFlightDate(String flightNumber, String flightDate);
}
