package com.dv.springms.chapter4.repository;

import com.dv.springms.chapter4.domain.Fare;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vitaliy on 4/17/17.
 */
public interface FaresRepository extends JpaRepository<Fare, Long> {
    Fare getFareByFlightNumberAndFlightDate(String flightNumber, String flightDate);
}
