package com.dv.springms.chapter4.controller;

import com.dv.springms.chapter4.component.FaresComponent;
import com.dv.springms.chapter4.domain.Fare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 4/17/17.
 */
@CrossOrigin
@RestController
@RequestMapping("/fares")
public class FaresController {

    FaresComponent faresComponent;

    @Autowired
    public FaresController(FaresComponent faresComponent) {
        this.faresComponent = faresComponent;
    }

    @RequestMapping("/get")
    Fare getFare(@RequestParam(value = "flightNumber") String flightNumber, @RequestParam(value = "flightDate") String flightDate) {
        return faresComponent.getFare(flightNumber,flightDate);
    }
}
