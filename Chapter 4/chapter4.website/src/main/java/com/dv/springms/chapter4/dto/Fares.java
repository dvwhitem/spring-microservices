package com.dv.springms.chapter4.dto;

/**
 * Created by vitaliy on 4/21/17.
 */
public class Fares {

    private Long id;

    private String fare;

    private  String currency;

    public Fares() {
    }

    public Fares(String fare, String currency) {
        this.fare = fare;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Fares{" +
                "id=" + id +
                ", fare='" + fare + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }
}
